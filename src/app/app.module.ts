import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MsalModule } from "@azure/msal-angular";
import { OAuthSettings } from 'src/environments/oauth';
import { AllUserWeeklyComponent } from './all-user-weekly/all-user-weekly.component';
import { TimezonePipe } from './timezone.pipe';
import { WeeklyViewComponent } from './weekly-view/weekly-view.component';
import { MatButtonModule } from "@angular/material/button";
import { MatIconModule } from "@angular/material/icon";

@NgModule({
  declarations: [
    AppComponent,
    AllUserWeeklyComponent,
    TimezonePipe,
    WeeklyViewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MsalModule.forRoot({
      clientID: OAuthSettings.appId,
      cacheLocation: 'localStorage'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
