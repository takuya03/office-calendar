import { Pipe, PipeTransform } from '@angular/core';
import { DateTimeTimeZone } from '@microsoft/microsoft-graph-types';

@Pipe({
  name: 'timezone'
})
export class TimezonePipe implements PipeTransform {

  transform(value: DateTimeTimeZone, ...args: any[]): Date {
    return new Date(value.dateTime);
  }

}
