import { TestBed } from '@angular/core/testing';

import { AzureOauthService } from './azure-oauth.service';

describe('AzureOauthService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AzureOauthService = TestBed.get(AzureOauthService);
    expect(service).toBeTruthy();
  });
});
