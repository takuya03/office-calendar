import { Event } from "@microsoft/microsoft-graph-types";
export interface EventData extends Event{
}

export class EventData implements Event {
    constructor(v:Event) {
        Object.assign(this, v);
    }

    get startDate():Date | undefined {
        if (this.start && this.start.dateTime) {
            return new Date(this.start.dateTime);
        }
        else {
            return undefined;
        }
    }

    get endDate():Date | undefined {
        if (this.end && this.end.dateTime) {
            return new Date(this.end.dateTime);
        }
        else {
            return undefined;
        }
    }
}
