import { User, Event } from '@microsoft/microsoft-graph-types';
import { EventData } from './event-data';
import { DayContainer } from './day-container';

export interface UserData extends User {

}

export class UserData {
    constructor(val?:User) {
        Object.assign(this, val);
    }

    private _events:Event[];
    set events(val:Event[]) {
        this._events = val;
        this.clearDaysEvents();
        this.addDaysEvents(val);
    }
    get events() {
        return this._events;
    }

    daysEvents:DayContainer<EventData[]>;

    /**
     * イベントデータを日付で整理する。
     * @param vals 
     */
    addDaysEvents(vals:Event[]) {
        if (!this.daysEvents) {
            this.daysEvents = new DayContainer<EventData[]>();
        }
        vals.forEach(element => {
            var event = new EventData(element);
            if (event.startDate) {
                var arraydata = this.daysEvents.get(event.startDate);
                if (!arraydata) {
                    arraydata = [];
                    this.daysEvents.add(event.startDate, arraydata);
                }

                if(!arraydata.find((v) => v.id === event.id)) {
                    arraydata.push(event);
                }
            }
        });
    }

    /**
     * イベントデータの削除
     */
    clearDaysEvents() {
        this.daysEvents = undefined;
    }
}
