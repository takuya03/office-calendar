import { EventData } from './event-data';

describe('EventData', () => {
  it('should create an instance', () => {
    expect(new EventData({})).toBeTruthy();
  });

  it('no start date', () => {
    var data = new EventData({
    });
    expect(data.startDate).toBeUndefined();
  });

  it('start date', () => {
    var data = new EventData({
      start: {
        dateTime: '2017-08-29T04:00:00.0000000',
        timeZone: 'Asia/Tokyo'
      }
    });
    var trueDate = new Date(2017, 7, 29, 4);
    expect(data.startDate.getTime()).toBe(trueDate.getTime());
  });

  it('no end date', () => {
    var data = new EventData({
    });
    expect(data.endDate).toBeUndefined();
  });

  it('end date', () => {
    var data = new EventData({
      end: {
        dateTime: '2017-08-29T04:00:00.0000000',
        timeZone: 'Asia/Tokyo'
      }
    });
    var trueDate = new Date(2017, 7, 29, 4);
    expect(data.endDate.getTime()).toBe(trueDate.getTime());
  });

});
