import { DayContainer } from './day-container';

describe('DayContainer', () => {
  it('should create an instance', () => {
    expect(new DayContainer()).toBeTruthy();
  });

  it('add data', () => {
    var container = new DayContainer<string>();
    container.add(new Date(2020, 0, 10), 'data');
    expect(container['2020-01-10']).toBe('data');
  });

  it('remove data', () => {
    var container = new DayContainer<string>();
    container.add(new Date(2020, 1, 10), 'data');
    expect(container['2020-02-10']).toBe('data');
    container.remove(new Date(2020, 1, 10));
    expect(container['2020-02-10']).toBeUndefined();
  });

  it('set new data', () => {
    var container = new DayContainer<string>();
    container.set(new Date(2020, 0, 1), 'data');
    expect(container['2020-01-01']).toBe('data');
  });

  it('replace data', () => {
    var container = new DayContainer<string>();
    container.set(new Date(2020, 0, 5), 'data');
    expect(container['2020-01-05']).toBe('data');
    container.set(new Date(2020, 0, 5), 'hogehoge');
    expect(container['2020-01-05']).toBe('hogehoge');
  });

  it('get data', () => {
    var container = new DayContainer<string>();
    container.set(new Date(2020, 11, 5), 'data');
    expect(container.get(new Date(2020, 11, 5))).toBe('data');
  })
});
