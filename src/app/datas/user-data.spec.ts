import { UserData } from './user-data';

describe('UserData', () => {
  it('should create an instance', () => {
    expect(new UserData()).toBeTruthy();
  });

  it('add event datas', () => {
    var user = new UserData();
    user.events = [
      {
        start: {
          dateTime: '2020-01-01T01:00:00.0000000',
          timeZone: 'Asia/Tokyo'
        },
        subject: 'test event1'
      },
      {
        start: {
          dateTime: '2020-01-01T02:00:00.0000000',
          timeZone: 'Asia/Tokyo'
        },
        subject: 'test event2'
      },
      {
        start: {
          dateTime: '2020-01-02T01:00:00.0000000',
          timeZone: 'Asia/Tokyo'
        },
        subject: 'test event3'
      },
    ];
    expect(user.daysEvents['2020-01-01'][0].subject).toBe('test event1');
    expect(user.daysEvents['2020-01-01'][1].subject).toBe('test event2');
    expect(user.daysEvents['2020-01-02'][0].subject).toBe('test event3');
  });
});
