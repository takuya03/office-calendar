export class DayContainer<T> {
    /**
     * データの追加
     * @param key 
     * @param value 
     */
    add(key:Date, value:T) {
        const keyStr = this.convertDate(key);
        if (this[keyStr] === undefined) {
            this[keyStr] = value;
        }
        else {
            throw new Error(`Already exists. key=${keyStr}`)
        }
    }

    /**
     * データの削除
     * @param key 
     */
    remove(key:Date) : T | undefined {
        const keyStr = this.convertDate(key);
        var old = this[keyStr];
        this[keyStr] = undefined;
        return old;
    }

    /**
     * データを設定
     * @param key 
     * @param value 
     */
    set(key:Date, value:T) {
        const keyStr = this.convertDate(key);
        this[keyStr] = value;
    }

    /**
     * データの取得
     * @param key 
     */
    get(key:Date) : T | undefined {
        const keyStr = this.convertDate(key);
        return this[keyStr];
    }

    private convertDate(date:Date) {
        return `${date.getFullYear()}-${('0' + (date.getMonth() + 1)).slice(-2)}-${('0' + date.getDate()).slice(-2)}`;
    }
}
