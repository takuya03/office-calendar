import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { tap, catchError } from "rxjs/operators";
import { MsalService } from '@azure/msal-angular';
import { OAuthSettings } from 'src/environments/oauth';
import { Client } from "@microsoft/microsoft-graph-client";

@Injectable({
  providedIn: 'root'
})
export class AzureOauthService {
  public authenticated:boolean = false;

  constructor(
    private msalService:MsalService
  ) { }

  getToken() {
    return from(this.msalService.acquireTokenSilent(OAuthSettings.scopes)).pipe(
      tap(res => {
        this.authenticated = true;
      }),
      catchError(err => {
        this.msalService.loginRedirect(OAuthSettings.scopes);
        return null
      })
    );
  }

  /**
   * MicrosoftGraphにアクセスするためのオブジェクトを取得
   */
  getGraphClient() {
    return Client.init({
      authProvider: (done) => {
        this.getToken().subscribe(token => {
          done(null, token)
        }, err => {
          console.error('トークンの取得に失敗しました。');
          done(err, null);
        })
      }
    });
  }

}
