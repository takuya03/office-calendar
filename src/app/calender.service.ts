import { Injectable } from '@angular/core';
import { AzureOauthService } from './azure-oauth.service';
import { from, Observable, concat, merge, of } from 'rxjs';
import { map, tap, mergeAll, flatMap } from 'rxjs/operators';
import { User } from "@microsoft/microsoft-graph-types";
import { Client } from '@microsoft/microsoft-graph-client';
import { UserData } from './datas/user-data';

@Injectable({
  providedIn: 'root'
})
export class CalenderService {

  users:User[];

  private isLoadEvents:boolean = false;

  constructor(
    private azureOauth:AzureOauthService
  ) { }


  /**
   * ユーザーリストの読み込み
   */
  loadUsersList(graphClient?:Client) {
    if (!graphClient) {
      graphClient = this.azureOauth.getGraphClient();
    }
    var promise = graphClient.api('/users')
      .filter('accountEnabled eq true')
      .select([
        'displayName',
        'id'
      ])
      .get();
    return from(promise).pipe(
      map(res => {
        var vals:User[] = res.value;
        return vals;
      }),
      tap(v => {
        this.users = v;
        this.isLoadEvents = false;
      })
    );
  }

  /**
   * ユーザーイベントの読み込み
   * @param startDate 
   * @param endDate 
   */
  loadUserEvents(user:User, startDate:Date, endDate:Date, optional?:{graphClient?:Client}) {
    var graphClient:Client;
    if (optional && optional.graphClient) {
      graphClient = optional.graphClient;
    }
    else {
      graphClient = this.azureOauth.getGraphClient()
    }
    return this.onLoadUserEvents(graphClient, user, startDate, endDate);
  }

  /**
   * イベント読み込みの発行
   * @param graphClient 
   * @param user 
   * @param startDate 
   * @param endDate 
   */
  private onLoadUserEvents(graphClient:Client, user:User, startDate:Date, endDate:Date) {
    return from(
      graphClient.api(`/users/${user.id}/events`)
      .header('Prefer', 'outlook.timezone="Asia/Tokyo"')
      .filter(`end/dateTime ge '${startDate.toISOString()}' and start/dateTime lt '${endDate.toISOString()}'`)
      .select([
        'id',
        'start',
        'end',
        'isAllDay',
        'subject'
      ])
      .get()
    ).pipe(
      map(v => {
        user.events = v.value;
        return user;
      })
    );
  }

  /**
   * 全ユーザーのリストを取得
   */
  loadAllUsersEvents(startDate:Date, endDate:Date) {
    return new Observable<User>(sub => {
      this.getUsersList().subscribe(
        users => {
          this.azureOauth.getToken().subscribe(token => {
            var gclient = Client.init({
              authProvider: (done) => done(null, token)
            });
            
            users.forEach(element => {
              this.onLoadUserEvents(gclient, element, startDate, endDate);
            });
            /*
            var list:Observable<Event[]>[] = [];
            users.forEach(element => {
              list.push(from(gclient.api(`/users/${element.id}/events`).get()).pipe(
                map(v => {
                  console.log(v);
                  element.events = v.value;
                  return v.value;
                })
              ));
            });

            merge(list).subscribe(res => {
              this.isLoadEvents = true;
              sub.next(users);
            });
            */
          })
        }
      )
    });
  }

  /**
   * ユーザーリストの取得
   */
  getUsersList(graphClient?:Client) {
    if (this.users) {
      return new Observable<User[]>((sub) => {
        sub.next(this.users)
      });
    }
    else {
      return this.loadUsersList(graphClient);
    }
  }

}
