import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserData } from '../datas/user-data';

export interface DateRange {
  start:Date;
  end:Date;
}

@Component({
  selector: 'app-weekly-view',
  templateUrl: './weekly-view.component.html',
  styleUrls: ['./weekly-view.component.scss']
})
export class WeeklyViewComponent implements OnInit {

  @Input()
  data:UserData[];

  @Input()
  startDate:Date = new Date(Date.now());

  @Input()
  dateCount:number = 5;

  dayStrList:string[];

  displayDays:string[];

  @Output()
  dateRangeChanged:EventEmitter<DateRange> = new EventEmitter<DateRange>();

  constructor() { }

  ngOnInit() {
    this.loadData();
  }

  moveDate(val:number) {
    this.startDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth(), this.startDate.getDate() + val);
    this.loadData();
  }

  private loadData() {
    var start = this.startDate;
    var dcnt = this.dateCount;
    var dayStrs = [];
    var dispDays = []
    for (let index = 0; index < dcnt; index++) {
      var tmp = new Date(start.getFullYear(), start.getMonth(), start.getDate() + index);
      dayStrs.push(`${tmp.getFullYear()}-${('0' + (tmp.getMonth() + 1)).slice(-2)}-${('0' + tmp.getDate()).slice(-2)}`);
      dispDays.push(`${tmp.getMonth() + 1}/${tmp.getDate()}`);
    }
    this.dayStrList = dayStrs;
    this.displayDays = dispDays;
    this.dateRangeChanged.next({
      start: start,
      end: new Date(start.getFullYear(), start.getMonth(), start.getDate() + (dcnt - 1))
    });

  }
}