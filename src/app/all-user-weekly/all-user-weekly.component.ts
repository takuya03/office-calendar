import { Component, OnInit } from '@angular/core';
import { AzureOauthService } from '../azure-oauth.service';
import { from } from 'rxjs';
import { CalenderService } from '../calender.service';
import { User } from '@microsoft/microsoft-graph-types';
import { UserData } from '../datas/user-data';
import { Client } from '@microsoft/microsoft-graph-client';
import { DateRange } from '../weekly-view/weekly-view.component';

@Component({
  selector: 'app-all-user-weekly',
  templateUrl: './all-user-weekly.component.html',
  styleUrls: ['./all-user-weekly.component.sass']
})
export class AllUserWeeklyComponent implements OnInit {

  users:UserData[] = [];

  lastDateRange:DateRange;

  constructor(
    private calenderService:CalenderService,
    private azureOauth:AzureOauthService
  ) { }

  ngOnInit() {
    this.azureOauth.getToken().subscribe(token => {
      var gclient = Client.init({
        authProvider: (done) => done(null, token)
      });
      this.calenderService.getUsersList(gclient).subscribe(userlist => {
        var users:UserData[] = [];
        userlist.forEach(user => {
          var data = new UserData(user);
          users.push(data);
        });
        this.users = users;
        if (this.lastDateRange) {
          this.onDateRange(this.lastDateRange);
        }
      });
    });
  }

  onDateRange(range:DateRange) {
    this.lastDateRange = range;
    if (!this.users) return;
    this.azureOauth.getToken().subscribe(token => {
      var gclient = Client.init({
        authProvider: (done) => done(null, token)
      });
      this.users.forEach(user => {
        var req:User = {id:user.id};
        this.calenderService.loadUserEvents(req, range.start, new Date(range.end.getFullYear(), range.end.getMonth(), range.end.getDate() + 1), {graphClient:gclient}).subscribe(res => {
          user.addDaysEvents(res.events);
        });
      });
    });
  }
}
