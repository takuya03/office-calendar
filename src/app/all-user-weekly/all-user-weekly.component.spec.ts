import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllUserWeeklyComponent } from './all-user-weekly.component';

describe('AllUserWeeklyComponent', () => {
  let component: AllUserWeeklyComponent;
  let fixture: ComponentFixture<AllUserWeeklyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllUserWeeklyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllUserWeeklyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
