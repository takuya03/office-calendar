import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllUserWeeklyComponent } from './all-user-weekly/all-user-weekly.component';


const routes: Routes = [
  { path: '', component: AllUserWeeklyComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
